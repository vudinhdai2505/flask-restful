from opentelemetry import trace
# from opentelemetry.exporter.otlp.proto.grpc.trace_exporter import \
#     OTLPSpanExporter
from opentelemetry.exporter.otlp.proto.http.trace_exporter import OTLPSpanExporter
from opentelemetry.sdk.trace import TracerProvider
from opentelemetry.sdk.trace.export import BatchSpanProcessor, ConsoleSpanExporter
from opentelemetry.instrumentation.flask import FlaskInstrumentor
from opentelemetry.instrumentation.sqlalchemy import SQLAlchemyInstrumentor
from opentelemetry.sdk.resources import SERVICE_NAME, Resource
endpoint = "https://fpt-api-tracing.prod.fmon.fptcloud.com/v1/traces"
# endpoint = "http://10.100.122.12:32404"
# header = "Authorization=basic ZnB0LWFwaTp0a3FJMm5JUUFPelZmTzgx,"
header = {"Authorization": "Basic ZnB0LWFwaTp0a3FJMm5JUUFPelZmTzgx"}
resource = Resource(attributes={
    SERVICE_NAME: "daivd_test3"
    })

class Tracer:
    def __init__(self, endpoint, header, resource):
        self.endpoint = endpoint
        self.header = header
        self.resource = resource
    def config(self):
        otlp_exporter = OTLPSpanExporter(endpoint=self.endpoint, headers=self.header)
        #resource = Resource(attributes={"service.name": "daivd_test2"})
        tracer = TracerProvider(resource=self.resource)
        trace.set_tracer_provider(tracer)
        span_processor = BatchSpanProcessor(otlp_exporter)
        tracer.add_span_processor(span_processor)
        tracer = trace.get_tracer(__name__)
        return tracer
OTEL_trace=Tracer(endpoint, header, resource)