FROM ubuntu:20.04
WORKDIR /root
RUN apt update && apt install -y python3-dev python3-pip python3-venv && pip3 install --upgrade pip
COPY . .
RUN pip3 install -r requirements.txt
EXPOSE 5000
CMD ["python3","main.py"]
